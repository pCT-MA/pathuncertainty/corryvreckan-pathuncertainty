![](doc/logo_small.png)

# Corryvreckan

This is a fork of the 
[corryvreckan repository](https://gitlab.cern.ch/corryvreckan/corryvreckan)
with changes dedicated to the path uncertainty research. It is full of prototype
code and considered deprecated as soon as Alex does not push to it any more.
Don't merge back to the official repository and don't ask them about problems
from here since they can't help you.
