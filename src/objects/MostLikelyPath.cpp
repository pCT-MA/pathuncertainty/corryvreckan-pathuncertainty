#include <numeric>

#include <Math/Integrator.h>
#include <TFile.h>
#include <TGraph.h>
#include <TTree.h>

#include "MostLikelyPath.hpp"

#include "objects/PhantomPath.hpp"

std::vector<double>
PolynomialWrapper::PolyFit(std::vector<double> const& X, std::vector<double> const& Y, std::size_t degree) {
    std::vector<double> coefficients(degree);
    std::size_t size = std::min(X.size(), Y.size());
    TGraph graph(static_cast<Int_t>(size), X.data(), Y.data());
    graph.LeastSquareFit(static_cast<Int_t>(degree), coefficients.data());
    return coefficients;
}

double PolynomialWrapper::operator()(double z) const {
    double result = 0;
    for(std::size_t i = 0; i < mCoefficients.size(); i++) {
        result += mCoefficients.at(i) * std::pow(z, i);
    }
    return result;
}

std::vector<double> const& PolynomialWrapper::GetCoefficients() const {
    return mCoefficients;
}

void PolynomialWrapper::SetCoefficients(std::vector<double> const& coefficients) {
    mCoefficients = coefficients;
}

double MomentumVelocity(double kineticEnergy, double restEnergy);
double MomentumVelocity(double kineticEnergy, double restEnergy) {
    return kineticEnergy * (kineticEnergy + 2 * restEnergy) / (kineticEnergy + 1 * restEnergy);
}

Geant4KinematicTerm::Geant4KinematicTerm(std::string const& inFileName, double restEnergy, std::size_t numberOfPlanes) {
    TFile inFile(inFileName.c_str());

    auto tree = dynamic_cast<TTree*>(inFile.Get(gTracingTreeName));
    double z;
    double energy;
    int event;
    int isPhantom;

    tree->SetBranchAddress(gZBranchName, &z, nullptr);
    tree->SetBranchAddress(gEnergyBranchName, &energy, nullptr);
    tree->SetBranchAddress(gEventBranchName, &event, nullptr);
    tree->SetBranchAddress(gIsPhantomBranchName, &isPhantom, nullptr);
    tree->SetBranchAddress(gEventBranchName, &event, nullptr);

    int currentEvent = -1;
    const auto entries = tree->GetEntries();
    if(entries > 0) {
        std::size_t j = 0;
        std::vector<double> ZMax(numberOfPlanes);
        std::vector<double> PVInv(numberOfPlanes);
        for(Int_t i = 0; i < entries; i++) {
            tree->GetEntry(i);
            if(isPhantom == 0)
                continue;
            if(currentEvent == -1 || currentEvent != event) {
                j = 0;
                currentEvent = event;
            }
            if(j >= numberOfPlanes) {
                throw std::runtime_error("Geant4KinematicTerm: wrong numberOfPlanes");
            }
            ZMax[j] = std::max(ZMax.at(j), z);
            PVInv[j] += 1 / std::pow(MomentumVelocity(energy, restEnergy), 2);
            ++j;
        }

        for(std::size_t k = 0; k < numberOfPlanes; k++) {
            PVInv[k] /= static_cast<double>(entries);
        }

        SetCoefficients(PolyFit(ZMax, PVInv, 5));
    }
    inFile.Close();
}

MostLikelyPath::MostLikelyPath(PolynomialWrapper* parameterisation, double charge, double radiationLength)
    : mParameterisation(std::move(parameterisation)), mCharge(charge), mRadiationLength(radiationLength) {}

MostLikelyPath::~MostLikelyPath() {
    delete mParameterisation;
}

double EygesMoment(double a, double b, double i, double charge, double X0, PolynomialWrapper const& parameterisation);
double EygesMoment(double a, double b, double i, double charge, double X0, PolynomialWrapper const& parameterisation) {
    constexpr double e0 = 13.6; // E0 = 13.6 / speed_of_light  # MeV/c
    const auto prefix = e0 * (1 + 0.038 * std::log((b - a) / X0));

    auto functor = [&](double z) { return std::pow(b - z, i) / X0 * parameterisation(z); };
    ROOT::Math::Integrator integrator(functor);

    return std::pow(charge * prefix, 2) * integrator.Integral(a, b);
}

Eigen::Vector2d MostLikelyPath::ProjectXY(BoundaryConditions const& boundaryConditions, double z) const {
    using Matrix = Eigen::Matrix2d;
    Eigen::Vector3d P0, P2;
    Eigen::Vector2d T0, T2;
    std::tie(P0, P2, T0, T2) = boundaryConditions;

    const Eigen::Vector2d xIn(P0.x(), T0.x());
    const Eigen::Vector2d xOut(P2.x(), T2.x());
    const Eigen::Vector2d yIn(P0.y(), T0.y());
    const Eigen::Vector2d yOut(P2.y(), T2.y());
    const double uIn = P0.z();
    const double uOut = P2.z();

    const auto eIn1 = EygesMoment(uIn, z, 1, mCharge, mRadiationLength, *mParameterisation);
    Matrix sigma1;
    sigma1 << EygesMoment(uIn, z, 2, mCharge, mRadiationLength, *mParameterisation), eIn1, eIn1,
        EygesMoment(uIn, z, 0, mCharge, mRadiationLength, *mParameterisation);

    const auto eOut1 = EygesMoment(z, uOut, 1, mCharge, mRadiationLength, *mParameterisation);
    Matrix sigma2;
    sigma2 << EygesMoment(z, uOut, 2, mCharge, mRadiationLength, *mParameterisation), eOut1, eOut1,
        EygesMoment(z, uOut, 0, mCharge, mRadiationLength, *mParameterisation);
    Matrix r0;
    r0 << 1, z - uIn, 0, 1;
    Matrix r1;
    r1 << 1, uOut - z, 0, 1;

    const auto r1Inv = r1.inverse();
    const auto cIn = r1Inv * sigma2 * (r1Inv * sigma2 + sigma1 * r1.transpose()).inverse() * r0;
    const auto cOut = sigma1 * (r1 * sigma1 + sigma2 * r1Inv.transpose()).inverse();

    auto xZ = cIn * xIn + cOut * xOut;
    auto yZ = cIn * yIn + cOut * yOut;
    return {xZ.x(), yZ.x()};
}
