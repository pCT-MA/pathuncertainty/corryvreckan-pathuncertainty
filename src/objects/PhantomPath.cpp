#include "PhantomPath.hpp"

namespace corryvreckan {

    std::size_t PhantomPath::GetSize() const { return mX.size(); }

    std::vector<double> const& PhantomPath::X() const { return mX; }
    std::vector<double> const& PhantomPath::Y() const { return mY; }
    std::vector<double> const& PhantomPath::Z() const { return mZ; }
    std::vector<double> const& PhantomPath::E() const { return mE; }

    void PhantomPath::Resize(std::size_t i) {
        mX.resize(i);
        mY.resize(i);
        mZ.resize(i);
        mE.resize(i);
    }
    void PhantomPath::Set(std::size_t i, double x, double y, double z, double e) {
        mX[i] = x;
        mY[i] = y;
        mZ[i] = z;
        mE[i] = e;
    }

    void PhantomPath::SetAllZero() {
        std::fill(mX.begin(), mX.end(), 0);
        std::fill(mY.begin(), mY.end(), 0);
        std::fill(mZ.begin(), mZ.end(), 0);
        std::fill(mE.begin(), mE.end(), 0);
    }
} // namespace corryvreckan
