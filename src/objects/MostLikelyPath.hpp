#ifndef CORRYVRECKAN_MOSTLIKELYPATH_H
#define CORRYVRECKAN_MOSTLIKELYPATH_H 1

#include <vector>

#include <Eigen/Geometry>

class PolynomialWrapper {
public:
    static std::vector<double> PolyFit(std::vector<double> const& X, std::vector<double> const& Y, std::size_t degree);

    double operator()(double z) const;
    std::vector<double> const& GetCoefficients() const;

    void SetCoefficients(std::vector<double> const& coefficients);

private:
    std::vector<double> mCoefficients;
};

class Geant4KinematicTerm : public PolynomialWrapper {
public:
    Geant4KinematicTerm(std::string const& inFileName, double restEnergy, std::size_t numberOfPlanes);

private:
    static constexpr const char* gTracingTreeName = "tracing";
    static constexpr const char* gZBranchName = "z";
    static constexpr const char* gEventBranchName = "event";
    static constexpr const char* gEnergyBranchName = "energy";
    static constexpr const char* gIsPhantomBranchName = "isPhantom";
};

class MostLikelyPath {
public:
    MostLikelyPath(PolynomialWrapper* parameterisation, double charge, double radiationLength = 361);
    virtual ~MostLikelyPath();

    typedef std::tuple<Eigen::Vector3d, Eigen::Vector3d, Eigen::Vector2d, Eigen::Vector2d> BoundaryConditions;

    Eigen::Vector2d ProjectXY(BoundaryConditions const& boundaryConditions, double z) const;

private:
    PolynomialWrapper* mParameterisation;
    double mCharge;
    double mRadiationLength;
};

#endif // CORRYVRECKAN_MOSTLIKELYPATH_H
