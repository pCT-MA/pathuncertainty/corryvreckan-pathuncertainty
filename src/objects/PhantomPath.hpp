#ifndef CORRYVRECKAN_PATH_H
#define CORRYVRECKAN_PATH_H 1

#include "Object.hpp"

namespace corryvreckan {

    class PhantomPath : public Object {
    public:
        PhantomPath() = default;
        static std::type_index getBaseType() { return typeid(PhantomPath); }

    public:
        std::size_t GetSize() const;
        std::vector<double> const& X() const;
        std::vector<double> const& Y() const;
        std::vector<double> const& Z() const;
        std::vector<double> const& E() const;

        void Resize(std::size_t);
        void Set(std::size_t i, double x, double y, double z, double e);
        void SetAllZero();

    private:
        std::vector<double> mX;
        std::vector<double> mY;
        std::vector<double> mZ;
        std::vector<double> mE;

        ClassDefOverride(PhantomPath, 1)
    };
    using PathVector = std::vector<std::shared_ptr<PhantomPath>>;
} // namespace corryvreckan

#endif